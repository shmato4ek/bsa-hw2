﻿using HW2.DAL.Data;
using HW2.DAL.Models;
using HW2.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2.DAL.UOW
{
    public class UnitOfWork : IUnitOfWork
    {
        private DataClass data;
        private UserRepository userRepository;
        private ProjectRepository projectRepository;
        private TaskRepository taskRepository;
        private TeamRepository teamRepository;
        public UnitOfWork ()
        {
            DataClass data = DataClass.GetInstance();
            this.data = data;

        }
        public IRepository<UserModel> Users
        {
            get
            {
                if (userRepository == null)
                {
                    userRepository = new UserRepository(data);
                }
                return userRepository;
            }
        }

        public IRepository<ProjectModel> Projects
        {
            get
            {
                if (projectRepository == null)
                {
                    projectRepository = new ProjectRepository(data);
                }
                return projectRepository;
            }
        }
         
        public IRepository<TaskModel> Tasks
        {
            get
            {
                if (taskRepository == null)
                {
                    taskRepository = new TaskRepository(data);
                }
                return taskRepository;
            }
        }

        public IRepository<TeamModel> Teams
        {
            get
            {
                if (teamRepository == null)
                {
                    teamRepository = new TeamRepository(data);
                }
                return teamRepository;
            }
        }
    }
}