﻿using HW2.DAL.Models;
using HW2.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2.DAL.UOW
{
    public interface IUnitOfWork
    {
        IRepository<ProjectModel> Projects { get; }
        IRepository<UserModel> Users { get; }
        IRepository<TaskModel> Tasks { get; }
        IRepository<TeamModel> Teams { get; }
    }
}
