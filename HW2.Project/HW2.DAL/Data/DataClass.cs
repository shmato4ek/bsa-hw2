﻿using HW2.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2.DAL.Data
{
    public class DataClass
    {
        private static DataClass data;
        public List<UserModel> Users { get; set; }
        public List<ProjectModel> Projects { get; set; }
        public List<TeamModel> Teams { get; set; }
        public List<TaskModel> Tasks { get; set; }
        private DataClass() 
        {
            Users = new List<UserModel>();
            Projects = new List<ProjectModel>();
            Teams = new List<TeamModel>();
            Tasks = new List<TaskModel>();
        }
        public static DataClass GetInstance()
        {
            if (data == null)
            {
                data = new DataClass();
                data.Users.Add(new UserModel { FirstName = "Alaya", LastName = "Andersen", Birthday = new DateTime(2000, 3, 25), Email = "anders78@gmail.com", Id = 0, RegisteredAt = new DateTime(2021, 12, 28), TeamId = 0 });
                data.Users.Add(new UserModel { FirstName = "Kuba", LastName = "Kay", Birthday = new DateTime(1992, 7, 4), Email = "kubakey92@gmail.com", Id = 1, RegisteredAt = new DateTime(2021, 12, 22), TeamId = 1 });
                data.Users.Add(new UserModel { FirstName = "Jevon", LastName = "Melia", Birthday = new DateTime(2013, 3, 11), Email = "jevon.melia@gmail.com", Id = 2, RegisteredAt = new DateTime(2017, 10, 18), TeamId = 1 });
                data.Users.Add(new UserModel { FirstName = "Ida", LastName = "Foster", Birthday = new DateTime(2002, 10, 21), Email = "f0st3rida@gmai.com", Id = 3, RegisteredAt = new DateTime(2019, 1, 20), TeamId = 2 });
                data.Users.Add(new UserModel { FirstName = "Cadence", LastName = "Watson", Birthday = new DateTime(2010, 8, 25), Email = "cadence12.com", Id = 3, RegisteredAt = new DateTime(2019, 1, 20), TeamId = 2 });

                data.Projects.Add(new ProjectModel { AuthorId = 0, TeamId = 0, CreatedAt = new DateTime(2021, 10, 11), DeadLine = new DateTime(2022, 1, 20), Description = "Platform for listening and saving user`s favourite music", Id = 0, Name = "Music platform" });
                data.Projects.Add(new ProjectModel { AuthorId = 1, TeamId = 1, CreatedAt = new DateTime(2022, 11, 2), DeadLine = new DateTime(2022, 2, 21), Description = "Web-site, where users can know more informayion about our company", Id = 1, Name = "Web-site for company" });
                data.Projects.Add(new ProjectModel { AuthorId = 2, TeamId = 1, CreatedAt = new DateTime(2021, 9, 21), DeadLine = new DateTime(2021, 12, 31), Description = "Creating new campus", Id = 2, Name = "KPI Campus" });
                data.Projects.Add(new ProjectModel { AuthorId = 3, TeamId = 2, CreatedAt = new DateTime(2021, 7, 12), DeadLine = new DateTime(2021, 12, 31), Description = "Site for study controll, based on Moodle", Id = 3, Name = "Moodle-based site" });
                data.Projects.Add(new ProjectModel { AuthorId = 4, TeamId = 2, CreatedAt = new DateTime(2021, 5, 2), DeadLine = new DateTime(2022, 3, 4), Description = "New shop need web-site", Id = 4, Name = "Web-site for new shop" });

                data.Tasks.Add(new TaskModel { CreatedAt = new DateTime(2021, 10, 11), Id = 0, Name = "Writing back", Description = "Write back for the platform", PerformerId = 4, ProjectId = 0, State = TaskStates.Started });
                data.Tasks.Add(new TaskModel { CreatedAt = new DateTime(2021, 1, 8), Id = 1, Name = "Create simple web-site for company", Description = "Writing back and front for new web-site", PerformerId = 3, ProjectId = 1, State = TaskStates.Started });
                data.Tasks.Add(new TaskModel { CreatedAt = new DateTime(2022, 1, 3), Id = 2, Name = "Find data", Description = "Get all information about students", FinishedAt = new DateTime(2021, 12, 30), PerformerId = 2, ProjectId = 2, State = TaskStates.Finished });
                data.Tasks.Add(new TaskModel { CreatedAt = new DateTime(2021, 10, 22), Id = 3, Name = "Write back", Description = "Write back for campus", FinishedAt = new DateTime(2021, 12, 30), PerformerId = 2, ProjectId = 2, State = TaskStates.Canceled });
                data.Tasks.Add(new TaskModel { CreatedAt = new DateTime(2021, 9, 21), Id = 4, Name = "Write front", Description = "Write front for campus", FinishedAt = new DateTime(2021, 11, 20), PerformerId = 1, ProjectId = 2, State = TaskStates.Canceled });
                data.Tasks.Add(new TaskModel { CreatedAt = new DateTime(2021, 7, 13), Id = 5, Name = "Create idea", Description = "Think about realization", FinishedAt = new DateTime(2021, 12, 30), PerformerId = 1, ProjectId = 3, State = TaskStates.Finished });
                data.Tasks.Add(new TaskModel { CreatedAt = new DateTime(2022, 1, 9), Id = 6, Name = "Talk with customer", Description = "Meet the customer and talk with him about the project", PerformerId = 0, ProjectId = 4, State = TaskStates.Started });
                data.Tasks.Add(new TaskModel { CreatedAt = new DateTime(2021, 5, 2), Id = 7, Name = "Implementing", Description = "Implement customer`s thinks", FinishedAt = new DateTime(2022, 1, 5), PerformerId = 0, ProjectId = 4, State = TaskStates.Canceled });

                data.Teams.Add(new TeamModel { CreatedAt = new DateTime(2021, 03, 18), Id = 0, Name = "Team A" });
                data.Teams.Add(new TeamModel { CreatedAt = new DateTime(2021, 03, 19), Id = 1, Name = "Team B" });
                data.Teams.Add(new TeamModel { CreatedAt = new DateTime(2021, 06, 20), Id = 2, Name = "Team C" });
            }
            return data;
        }
    }
}
