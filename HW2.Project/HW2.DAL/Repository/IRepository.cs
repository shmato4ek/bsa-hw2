﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2.DAL.Repository
{
    public interface IRepository<T>
        where T : class
    {
        T Create(T item);
        List<T> GetAll();
        T Get(int id);
        T Update(T item);
        void Delete(int id);
    }
}
