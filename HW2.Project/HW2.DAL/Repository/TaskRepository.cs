﻿using HW2.DAL.Data;
using HW2.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2.DAL.Repository
{
    public class TaskRepository : IRepository<TaskModel>
    {
        private DataClass data;
        public TaskRepository(DataClass data)
        {
            this.data = data;
        }
        public TaskModel Create(TaskModel item)
        {
            data.Tasks.Add(item);
            return data.Tasks.Where(task => task.Id == item.Id).FirstOrDefault();
        }

        public void Delete(int id)
        {
            TaskModel task = data.Tasks.Where(task => task.Id == id).FirstOrDefault();
            data.Tasks.Remove(task);
        }

        public TaskModel Get(int id)
        {
            return data.Tasks.Where(task => task.Id == id).FirstOrDefault();
        }

        public List<TaskModel> GetAll()
        {
            return data.Tasks;
        }

        public TaskModel Update(TaskModel item)
        {
            var task = data.Tasks.Where(task => task.Id == item.Id).FirstOrDefault();
            if(task != null)
            {
                var id = task.Id;
                data.Tasks.Remove(task);
                data.Tasks.Insert(id, item);
            }
            return data.Tasks.Where(task => task.Id == item.Id).FirstOrDefault();
        }
    }
}