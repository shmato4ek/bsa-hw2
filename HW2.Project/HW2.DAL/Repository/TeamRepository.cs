﻿using HW2.DAL.Data;
using HW2.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2.DAL.Repository
{
    public class TeamRepository : IRepository<TeamModel>
    {
        private DataClass data;
        public TeamRepository(DataClass data)
        {
            this.data = data;
        }
        public TeamModel Create(TeamModel item)
        {
            data.Teams.Add(item);
            return data.Teams.Where(team => team.Id == item.Id).FirstOrDefault();
        }

        public void Delete(int id)
        {
            TeamModel team = data.Teams.Where(team => team.Id == id).FirstOrDefault();
            data.Teams.Remove(team);
        }

        public TeamModel Get(int id)
        {
            return data.Teams.Where(team => team.Id == id).FirstOrDefault();
        }

        public List<TeamModel> GetAll()
        {
            return data.Teams;
        }

        public TeamModel Update(TeamModel item)
        {
            var team = data.Teams.Where(team => team.Id == item.Id).FirstOrDefault();
            if (team != null)
            {
                var id = team.Id;
                data.Teams.Remove(team);
                data.Teams.Insert(id, item);
            }
            return data.Teams.Where(team => team.Id == item.Id).FirstOrDefault();
        }
    }
}
