﻿using HW2.DAL.Data;
using HW2.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2.DAL.Repository
{
    public class ProjectRepository : IRepository<ProjectModel>
    {
        private DataClass data;
        public ProjectRepository(DataClass data)
        {
            this.data = data;
        }
        public ProjectModel Create(ProjectModel item)
        {
            data.Projects.Add(item);
            return data.Projects.Where(project => project.Id == item.Id).FirstOrDefault();
        }

        public void Delete(int id)
        {
            ProjectModel project = data.Projects.Where(project => project.Id == id).FirstOrDefault();
            data.Projects.Remove(project);
        }

        public ProjectModel Get(int id)
        {
            return data.Projects.Where(project => project.Id == id).FirstOrDefault();
        }

        public List<ProjectModel> GetAll()
        {
            return data.Projects;
        }

        public ProjectModel Update(ProjectModel item)
        {
            var project = data.Projects.Where(project => project.Id == item.Id).FirstOrDefault();
            if (project != null)
            {
                var id = project.Id;
                data.Projects.Remove(project);
                data.Projects.Insert(id, item);
            }
            return data.Projects.Where(project => project.Id == item.Id).FirstOrDefault();
        }
    }
}
