﻿using HW2.DAL.Data;
using HW2.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2.DAL.Repository
{
    public class UserRepository : IRepository<UserModel>
    {
        private DataClass data;
        public UserRepository(DataClass data)
        {
            this.data = data;
        }
        public UserModel Create(UserModel item)
        {
            data.Users.Add(item);
            return data.Users.Where(user => user.Id == item.Id).FirstOrDefault();
        }

        public void Delete(int id)
        {
            UserModel user = data.Users.Where(user => user.Id == id).FirstOrDefault();
            data.Users.Remove(user);
        }

        public UserModel Get(int id)
        {
            return data.Users.Where(user => user.Id == id).FirstOrDefault();
        }

        public List<UserModel> GetAll()
        {
            return data.Users;
        }

        public UserModel Update(UserModel item)
        {
            var user = data.Users.Where(user => user.Id == item.Id).FirstOrDefault();
            if (user != null)
            {
                var id = user.Id;
                data.Users.Remove(user);
                data.Users.Insert(id, item);
            }
            return data.Users.Where(team => team.Id == item.Id).FirstOrDefault();
        }
    }
}
