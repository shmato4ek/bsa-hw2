﻿using Common.DTO;
using HW2.BLL.Structs;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace HW2.ConsoleProject
{
    class Program
    {
        static void Main(string[] args)
        {
            bool flag = true;
            using (HttpClient client = new HttpClient())
            {
                while (flag)
                {
                    Console.WriteLine("*********************************************************************************************************");
                    Console.WriteLine("\tWelcome to my console :)\n\tThere is list of all tasks:\n\n");
                    Console.WriteLine("1. Tasks.\n" +
                        "2. Projects.\n" +
                        "3. Teams.\n" +
                        "4. Users.\n" +
                        "5. LINQ\n" +
                        "0. Exit\n");
                    Console.Write("Write the number of task: ");
                    int answer1 = Int32.Parse(Console.ReadLine());
                    switch (answer1)
                    {
                        case 1:
                            Console.WriteLine("\n\n\tTasks:\n");
                            Console.WriteLine("1. Get all.\n" +
                                "2. Get task by id.\n" +
                                "3. Update task.\n" +
                                "4. Delete task.\n" +
                                "5. Add task.\n");
                            Console.Write(" Write number: ");
                            int answer2 = Int32.Parse(Console.ReadLine());
                            switch (answer2)
                            {
                                case 1:
                                    Console.WriteLine("\n\nAll tasks:\n\n");
                                    var allTasksJson = client.GetAsync("https://localhost:44371/" + "api/task");
                                    var allTasks = JsonConvert.DeserializeObject<List<TaskDTO>>(allTasksJson.Result.Content.ReadAsStringAsync().Result);
                                    foreach (var item in allTasks)
                                    {
                                        Console.WriteLine("Id: " + item.Id + ", name: " + item.Name);
                                    }
                                    break;
                                case 2:
                                    Console.Write("\n\nWrite id: ");
                                    int taskId = Int32.Parse(Console.ReadLine());
                                    try
                                    {
                                        var taskJson = client.GetAsync("https://localhost:44371/" + "api/task/" + taskId.ToString());
                                        var task = JsonConvert.DeserializeObject<TaskDTO>(taskJson.Result.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine("Your task is " + task.Id.ToString() + ". " + task.Name);
                                    }
                                    catch(Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 3:
                                    Console.Write("\n\nWrite task id: ");
                                    int taskId2 = Int32.Parse(Console.ReadLine());
                                    Console.Write("\nWrite name: ");
                                    string taskName1 = Console.ReadLine();
                                    Console.Write("\nWrite description: ");
                                    string taskDescription1 = Console.ReadLine();
                                    var task2 = new TaskDTO { Id = taskId2, Name = taskName1, Description = taskDescription1, CreatedAt = DateTime.Now };
                                    var taskJson2 = JsonConvert.SerializeObject(task2);
                                    var taskRequest1 = new StringContent(taskJson2, Encoding.UTF8, "application/json");
                                    try
                                    {
                                        var taskResponseJson1 = client.PutAsync("https://localhost:44371/" + "api/task", taskRequest1).Result.Content.ReadAsStringAsync().Result;
                                        var taskResponse1 = JsonConvert.DeserializeObject<TaskDTO>(taskResponseJson1);
                                        Console.WriteLine("Task was sucessfully updated! Task id: " + taskResponse1.Id.ToString() + ". " + taskResponse1.Name);
                                    }
                                    catch(Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 4:
                                    Console.Write("\n\nWrite id: ");
                                    int taskId3 = Int32.Parse(Console.ReadLine());
                                    try
                                    {
                                        client.DeleteAsync("https://localhost:44371/" + "api/task/" + taskId3.ToString());
                                        Console.WriteLine("Task with id: {0} was sucessfully deleted!", taskId3);
                                    }
                                    catch(Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 5:
                                    Console.Write("\n\nWrite id: ");
                                    var taskId4 = Int32.Parse(Console.ReadLine());
                                    Console.Write("\nWrite name: ");
                                    var taskName2 = Console.ReadLine();
                                    Console.Write("\nWtite description: ");
                                    var taskDescription2 = Console.ReadLine();
                                    var taskJson3 = JsonConvert.SerializeObject(new TaskDTO { CreatedAt = DateTime.Now, Id = taskId4, Name = taskName2, Description = taskDescription2 });
                                    var task3 = new StringContent(taskJson3, Encoding.UTF8, "application/json");
                                    try
                                    {
                                        var taskResponseJson3 = client.PostAsync("https://localhost:44371/" + "api/task", task3);
                                        var taskResponse3 = JsonConvert.DeserializeObject<TaskDTO>(taskResponseJson3.Result.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine("Task with id {0} and name {1} was sucessfully posted!", taskResponse3.Id.ToString(), taskResponse3.Name);
                                    }
                                    catch(Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 2:
                            Console.WriteLine("\n\n\tProjects:\n");
                            Console.WriteLine("1. Get all.\n" +
                                "2. Get project by id.\n" +
                                "3. Update project.\n" +
                                "4. Delete project.\n" +
                                "5. Add project.\n");
                            Console.Write(" Write number: ");
                            int answer3 = Int32.Parse(Console.ReadLine());
                            switch (answer3)
                            {
                                case 1:
                                    Console.WriteLine("\n\nAll projects:\n\n");
                                    var allProjectsJson = client.GetAsync("https://localhost:44371/" + "api/project");
                                    var allProjects = JsonConvert.DeserializeObject<List<ProjectDTO>>(allProjectsJson.Result.Content.ReadAsStringAsync().Result);
                                    foreach (var item in allProjects)
                                    {
                                        Console.WriteLine("Id: " + item.Id + ", name: " + item.Name);
                                    }
                                    break;
                                case 2:
                                    Console.Write("\n\nWrite id: ");
                                    int projectId = Int32.Parse(Console.ReadLine());
                                    try
                                    {
                                        var projectJson = client.GetAsync("https://localhost:44371/" + "api/project/" + projectId.ToString());
                                        var project = JsonConvert.DeserializeObject<ProjectDTO>(projectJson.Result.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine("Your project is " + project.Id.ToString() + ". " + project.Name);
                                    }
                                    catch(Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 3:
                                    Console.Write("\n\nWrite project id: ");
                                    int projectId2 = Int32.Parse(Console.ReadLine());
                                    Console.Write("\nWrite name: ");
                                    string projectName1 = Console.ReadLine();
                                    Console.Write("\nWrite description: ");
                                    string projectDescription1 = Console.ReadLine();
                                    var project2 = new TaskDTO { Id = projectId2, Name = projectName1, Description = projectDescription1, CreatedAt = DateTime.Now };
                                    var projectJson2 = JsonConvert.SerializeObject(project2);
                                    var projectRequest1 = new StringContent(projectJson2, Encoding.UTF8, "application/json");
                                    try
                                    {
                                        var projectResponseJson1 = client.PutAsync("https://localhost:44371/" + "api/project", projectRequest1).Result.Content.ReadAsStringAsync().Result;
                                        var projectResponse1 = JsonConvert.DeserializeObject<ProjectDTO>(projectResponseJson1);
                                        Console.WriteLine("Project was sucessfully updated! Project id: " + projectResponse1.Id.ToString() + ". " + projectResponse1.Name);
                                    }
                                    catch(Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 4:
                                    Console.Write("\n\nWrite id: ");
                                    int projectId3 = Int32.Parse(Console.ReadLine());
                                    try
                                    {
                                        var projectResponseJson2 = client.DeleteAsync("https://localhost:44371/" + "api/project/" + projectId3.ToString());
                                        Console.WriteLine("Project with id: {0} was sucessfully deleted!", projectId3);
                                    }
                                    catch(Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 5:
                                    Console.Write("\n\nWrite id: ");
                                    var projectId4 = Int32.Parse(Console.ReadLine());
                                    Console.Write("\nWrite name: ");
                                    var projectName2 = Console.ReadLine();
                                    Console.Write("\nWtite description: ");
                                    var projectDescription2 = Console.ReadLine();
                                    var projectJson3 = JsonConvert.SerializeObject(new ProjectDTO { CreatedAt = DateTime.Now, Id = projectId4, Name = projectName2, Description = projectDescription2 });
                                    var project3 = new StringContent(projectJson3, Encoding.UTF8, "application/json");
                                    try
                                    {
                                        var projectResponseJson3 = client.PostAsync("https://localhost:44371/" + "api/project", project3);
                                        var projectResponse3 = JsonConvert.DeserializeObject<ProjectDTO>(projectResponseJson3.Result.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine("Project with id {0} and name {1} was sucessfully posted!", projectResponse3.Id.ToString(), projectResponse3.Name);
                                    }
                                    catch(Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 3:
                            Console.WriteLine("\n\n\tTeasm:\n");
                            Console.WriteLine("1. Get all.\n" +
                                "2. Get team by id.\n" +
                                "3. Update team.\n" +
                                "4. Delete team.\n" +
                                "5. Add team.\n");
                            Console.Write(" Write number: ");
                            int answer4 = Int32.Parse(Console.ReadLine());
                            switch (answer4)
                            {
                                case 1:
                                    Console.WriteLine("\n\nAll teams:\n\n");
                                    var allTeamsJson = client.GetAsync("https://localhost:44371/" + "api/team");
                                    var allTeams = JsonConvert.DeserializeObject<List<TeamDTO>>(allTeamsJson.Result.Content.ReadAsStringAsync().Result);
                                    foreach (var item in allTeams)
                                    {
                                        Console.WriteLine("Id: " + item.Id + ", name: " + item.Name);
                                    }
                                    break;
                                case 2:
                                    Console.Write("\n\nWrite id: ");
                                    int teamId = Int32.Parse(Console.ReadLine());
                                    try
                                    {
                                        var teamJson = client.GetAsync("https://localhost:44371/" + "api/team/" + teamId.ToString());
                                        var team = JsonConvert.DeserializeObject<TeamDTO>(teamJson.Result.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine("Your team is " + team.Id.ToString() + ". " + team.Name);
                                    }
                                    catch(Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 3:
                                    Console.Write("\n\nWrite team id: ");
                                    int teamId2 = Int32.Parse(Console.ReadLine());
                                    Console.Write("\nWrite name: ");
                                    string teamName1 = Console.ReadLine();
                                    var team2 = new TeamDTO { Id = teamId2, Name = teamName1, CreatedAt = DateTime.Now };
                                    var teamJson2 = JsonConvert.SerializeObject(team2);
                                    var teamRequest1 = new StringContent(teamJson2, Encoding.UTF8, "application/json");
                                    try
                                    {
                                        var teamResponseJson1 = client.PutAsync("https://localhost:44371/" + "api/team", teamRequest1).Result.Content.ReadAsStringAsync().Result;
                                        var teamResponse1 = JsonConvert.DeserializeObject<TeamDTO>(teamResponseJson1);
                                        Console.WriteLine("Team was sucessfully updated! Team id: " + teamResponse1.Id.ToString() + ". " + teamResponse1.Name);
                                    }
                                    catch(Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 4:
                                    Console.Write("\n\nWrite id: ");
                                    int teamId3 = Int32.Parse(Console.ReadLine());
                                    try
                                    {
                                        var teamResponseJson2 = client.DeleteAsync("https://localhost:44371/" + "api/team/" + teamId3.ToString());
                                        Console.WriteLine("Team with id: {0} was sucessfully deleted!", teamId3);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 5:
                                    Console.Write("\n\nWrite id: ");
                                    var teamId4 = Int32.Parse(Console.ReadLine());
                                    Console.Write("\nWrite name: ");
                                    var teamName2 = Console.ReadLine();
                                    var teamJson3 = JsonConvert.SerializeObject(new TeamDTO { CreatedAt = DateTime.Now, Id = teamId4, Name = teamName2 });
                                    var team3 = new StringContent(teamJson3, Encoding.UTF8, "application/json");
                                    try
                                    {
                                        var teamResponseJson3 = client.PostAsync("https://localhost:44371/" + "api/team", team3);
                                        var teamResponse3 = JsonConvert.DeserializeObject<TeamDTO>(teamResponseJson3.Result.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine("Team with id {0} and name {1} was sucessfully posted!", teamResponse3.Id.ToString(), teamResponse3.Name);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 4:
                            Console.WriteLine("1. Get all.\n" +
                                "2. Get user by id.\n" +
                                "3. Update user.\n" +
                                "4. Delete user.\n" +
                                "5. Add user.\n");
                            Console.Write(" Write number: ");
                            int answer5 = Int32.Parse(Console.ReadLine());
                            switch (answer5)
                            {
                                case 1:
                                    Console.WriteLine("\n\nAll users:\n\n");
                                    var allUsersJson = client.GetAsync("https://localhost:44371/" + "api/user");
                                    var allUsers = JsonConvert.DeserializeObject<List<UserDTO>>(allUsersJson.Result.Content.ReadAsStringAsync().Result);
                                    foreach (var item in allUsers)
                                    {
                                        Console.WriteLine("Id: " + item.Id + ", name: " + item.FirstName + " " + item.LastName);
                                    }
                                    break;
                                case 2:
                                    Console.Write("\n\nWrite id: ");
                                    int userId = Int32.Parse(Console.ReadLine());
                                    try
                                    {
                                        var userJson = client.GetAsync("https://localhost:44371/" + "api/user/" + userId.ToString());
                                        var user = JsonConvert.DeserializeObject<UserDTO>(userJson.Result.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine("Your user is " + user.Id.ToString() + ". " + user.FirstName + " " + user.LastName);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 3:
                                    Console.Write("\n\nWrite user id: ");
                                    int userId2 = Int32.Parse(Console.ReadLine());
                                    Console.Write("\nWrite first name: ");
                                    string userFirstName1 = Console.ReadLine();
                                    Console.Write("\nWrite last name: ");
                                    string userLastName1 = Console.ReadLine();
                                    var user2 = new UserDTO { Id = userId2, FirstName = userFirstName1, LastName = userLastName1 };
                                    var userJson2 = JsonConvert.SerializeObject(user2);
                                    var userRequest1 = new StringContent(userJson2, Encoding.UTF8, "application/json");
                                    try
                                    {
                                        var userResponseJson1 = client.PutAsync("https://localhost:44371/" + "api/user", userRequest1).Result.Content.ReadAsStringAsync().Result;
                                        var userResponse1 = JsonConvert.DeserializeObject<UserDTO>(userResponseJson1);
                                        Console.WriteLine("User was sucessfully updated! User id: " + userResponse1.Id.ToString() + ". " + userResponse1.FirstName + " " + userResponse1.LastName);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 4:
                                    Console.Write("\n\nWrite id: ");
                                    int userId3 = Int32.Parse(Console.ReadLine());
                                    try
                                    {
                                        var userResponseJson2 = client.DeleteAsync("https://localhost:44371/" + "api/user/" + userId3.ToString());
                                        Console.WriteLine("User with id: {0} and was sucessfully deleted!", userId3);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                case 5:
                                    Console.Write("\n\nWrite id: ");
                                    var userId4 = Int32.Parse(Console.ReadLine());
                                    Console.Write("\nWrite first name: ");
                                    var userFirstName2 = Console.ReadLine();
                                    Console.Write("\nWrite last name: ");
                                    var userLastName2 = Console.ReadLine();
                                    var userJson3 = JsonConvert.SerializeObject(new UserDTO { Id = userId4, FirstName = userFirstName2, LastName = userLastName2 });
                                    var user3 = new StringContent(userJson3, Encoding.UTF8, "application/json");
                                    try
                                    {
                                        var userResponseJson3 = client.PostAsync("https://localhost:44371/" + "api/user", user3);
                                        var userResponse3 = JsonConvert.DeserializeObject<UserDTO>(userResponseJson3.Result.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine("User with id {0} and name {1} {2} was sucessfully posted!", userResponse3.Id.ToString(), userResponse3.FirstName, userResponse3.LastName);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 5:
                            Console.WriteLine("\n\n\tLINQ\n\n");
                            Console.WriteLine("1. Ammount of tasks of concrete user`s project.\n" +
                                "2. List of concrete user`s tasks with length < 45 symbols.\n" +
                                "3. List of concrete user`s tasks that were completed in 2021.\n" +
                                "4. List of teams with 10 and more y.o. users, sorted by time of registration discending and grouped by teams.\n" +
                                "5. List of users sorted by first name wits tasks, sorted by length of name discending.\n" +
                                "6. Special struct#1.\n" +
                                "7. Special struct#2.\n" +
                                "0. Exit\n" +
                                "*********************************************************************************************************\n\n");
                            Console.Write("\tWrite the number of task: ");
                            int answer = Int32.Parse(Console.ReadLine());
                            switch (answer)
                            {
                                case 1:
                                    Console.WriteLine("\n\n\t1. Ammount of tasks of concrete user`s project.\n\n");
                                    Console.Write("Write id of user: ");
                                    int userIdTask1 = Int32.Parse(Console.ReadLine());
                                    Console.WriteLine();
                                    try
                                    {
                                        var task1Json = client.GetAsync("https://localhost:44371/api/linq/user/" + userIdTask1.ToString() + "/tasks/amount");
                                        var task1 = JsonConvert.DeserializeObject<List<ProjectDictionaryDTO>>(task1Json.Result.Content.ReadAsStringAsync().Result);
                                        foreach (var item in task1)
                                        {
                                            Console.WriteLine("Project name: {0}, ammount of tasks: {1}", item.Key.Name, item.Value.ToString());
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    Console.WriteLine();
                                    break;
                                default:
                                    break;
                                case 2:
                                    Console.WriteLine("\n\n\t2. List of concrete user`s tasks with length < 45 symbols.\n\n");
                                    Console.Write("Write id of user: ");
                                    int userIdTask2 = Int32.Parse(Console.ReadLine());
                                    Console.WriteLine();
                                    var task2Json = client.GetAsync("https://localhost:44371/api/linq/user/" + userIdTask2.ToString() + "/tasks");
                                    var task2 = JsonConvert.DeserializeObject<List<TaskDTO>>(task2Json.Result.Content.ReadAsStringAsync().Result);
                                    try
                                    {
                                        foreach (var item in task2)
                                        {
                                            Console.WriteLine("Task name: {0}({1}).", item.Name, item.Description);
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    Console.WriteLine();
                                    break;
                                case 3:
                                    Console.WriteLine("\n\n\t3. List of concrete user`s tasks that were completed in 2021.\n\n");
                                    Console.Write("Write id of user: ");
                                    int userIdTask3 = Int32.Parse(Console.ReadLine());
                                    Console.WriteLine();
                                    try
                                    {
                                        var task3Json = client.GetAsync("https://localhost:44371/api/linq/user/" + userIdTask3.ToString() + "/tasks/finished");
                                        var task3 = JsonConvert.DeserializeObject<List<TaskDictionaryDTO>>(task3Json.Result.Content.ReadAsStringAsync().Result);
                                        foreach (var item in task3)
                                        {
                                            Console.WriteLine("Id: {0}, name: {1}", item.Key.ToString(), item.Value);
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    Console.WriteLine();
                                    break;
                                case 4:
                                    Console.WriteLine("\n\n\t4. List of teams with 10 and more y.o. users, sorted by time of registration discending and grouped by teams.\n\n");
                                    var task4Json = client.GetAsync("https://localhost:44371/api/linq/teams/older-then10");
                                    var task4 = JsonConvert.DeserializeObject<List<TeamWithUsersStruct>>(task4Json.Result.Content.ReadAsStringAsync().Result);
                                    foreach (var item in task4)
                                    {
                                        Console.WriteLine("\tTeam id: {0}, team name: {1}, users: ", item.Id, item.TeamName);
                                        foreach (var user in item.Users)
                                        {
                                            Console.Write("{0} {1},", user.LastName, user.FirstName);
                                        }
                                        Console.WriteLine("\n");
                                    }
                                    Console.WriteLine();
                                    break;
                                case 5:
                                    Console.WriteLine("\n\n\t5. List of users sorted by first name wits tasks, sorted by length of name discending.\n\n");
                                    var task5Json = client.GetAsync("https://localhost:44371/api/linq/users/sorted");
                                    var task5 = JsonConvert.DeserializeObject<List<UserDictionaryDTO>>(task5Json.Result.Content.ReadAsStringAsync().Result);
                                    foreach (var item in task5)
                                    {
                                        Console.WriteLine("\tUser name: {0} {1}, tasks:", item.Key.FirstName, item.Key.LastName);
                                        foreach (var task in item.Value)
                                        {
                                            Console.Write("{0}, ", task.Name);
                                        }
                                        Console.WriteLine("\n");
                                    }
                                    Console.WriteLine();
                                    break;
                                case 6:
                                    Console.WriteLine("\n\n\t6. Special struct#1.\n\n");
                                    Console.Write("Write id of user: ");
                                    int userIdTask6 = Int32.Parse(Console.ReadLine());
                                    Console.WriteLine();
                                    try
                                    {
                                        var task6Json = client.GetAsync("https://localhost:44371/api/linq/user/" + userIdTask6.ToString() + "/info");
                                        var task6 = JsonConvert.DeserializeObject<UserStructDTO>(task6Json.Result.Content.ReadAsStringAsync().Result);
                                        Console.WriteLine("UserId: {0}, user name: {1} {2}.", task6.User.Id, task6.User.FirstName, task6.User.LastName);
                                        Console.WriteLine("Last user`s project: {0}({1}) created at: {2}",task6.LastProject.Name, task6.LastProject.Description, task6.LastProject.CreatedAt.ToString());
                                        Console.WriteLine("Ammount of tasks of last user`s project: {0}.", task6.AmmountOfTaskInLastProject.ToString());
                                        Console.WriteLine("Ammount of not finished or canceled tasks: {0}.", task6.AmmoutOfNotFinishedOrClosedTasks.ToString());
                                        Console.WriteLine("The longest user`s task: {0}({1})", task6.TheLongestTask.Name, task6.TheLongestTask.Description);
                                        Console.WriteLine();
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e.Message);
                                    }
                                    Console.WriteLine();
                                    break;
                                case 7:
                                    Console.WriteLine("\n\n\t7. Special struct#2.\n\n");
                                    var task7Json = client.GetAsync("https://localhost:44371/api/linq/projects/info");
                                    var task7 = JsonConvert.DeserializeObject<List<ProjectStruct>>(task7Json.Result.Content.ReadAsStringAsync().Result);
                                    foreach (var project in task7)
                                    {
                                        Console.WriteLine("\tProject: {0}. {1}({2}).", project.Project.Id.ToString(), project.Project.Name, project.Project.Description);
                                        if (project.theLongestTask != null)
                                        {
                                            Console.WriteLine("The longest task: {0}({1}).", project.theLongestTask.Name, project.theLongestTask.Description);
                                            Console.WriteLine("The shortest task: {0}({1}).", project.theShortestTask.Name, project.theShortestTask.Description);
                                        }
                                        else
                                        {
                                            Console.WriteLine("This project doesn`t contain any task");
                                        }
                                        if (project.ammountOfUsers != 0)
                                        {
                                            Console.WriteLine("The ammount of users in project`s team: {0}", project.ammountOfUsers.ToString());
                                        }
                                        else
                                        {
                                            Console.WriteLine("Description of the project <= 20 symbols and ammount of project`s task >= 3.");
                                        }
                                        Console.WriteLine("\n");
                                    }
                                    Console.WriteLine();
                                    break;

                            }
                            break;
                        case 0:
                            flag = false;
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
}
