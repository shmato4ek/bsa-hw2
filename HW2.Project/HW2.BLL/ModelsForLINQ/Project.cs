﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW1.BLL.ModelsForLINQ
{
    public class Project
    {
        public List<ProjectTask> Tasks { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DeadLine { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
