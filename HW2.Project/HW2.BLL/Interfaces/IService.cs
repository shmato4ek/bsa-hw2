﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2.BLL.Interfaces
{
    public interface IService<T>
    {
        List<T> GetAll();
        T Get(int id);
        T Update(T item);
        T Add(T item);
        void Delete(int id);
    }
}
