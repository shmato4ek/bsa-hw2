﻿using Common.DTO;
using HW1.BLL.ModelsForLINQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2.BLL.Structs
{
    public struct UserStruct
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int AmmountOfTaskInLastProject { get; set; }
        public int AmmoutOfNotFinishedOrClosedTasks { get; set; }
        public TaskDTO TheLongestTask { get; set; }
    }
}
