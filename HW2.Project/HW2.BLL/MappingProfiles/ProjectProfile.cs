﻿using AutoMapper;
using Common.DTO;
using HW2.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectModel, ProjectDTO>();
            CreateMap<ProjectDTO, ProjectModel>();
        }
    }
}
