﻿using AutoMapper;
using Common.DTO;
using HW1.BLL.ModelsForLINQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2.BLL.MappingProfiles
{
    public class TaskForLinqProfile : Profile
    {
        public TaskForLinqProfile()
        {
            CreateMap<ProjectTask, TaskDTO>().ForMember(task => task.PerformerId, src => src.MapFrom(member => member.Performer.Id));
            CreateMap<TaskDTO, ProjectTask>();
        }
    }
}
