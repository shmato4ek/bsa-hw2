﻿using AutoMapper;
using Common.DTO;
using HW2.BLL.Interfaces;
using HW2.BLL.Services.Abstract;
using HW2.DAL.Data;
using HW2.DAL.Models;
using HW2.DAL.UOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2.BLL.Services
{
    public class UserService : BaseService, IService<UserDTO>
    {
        public UserService(IMapper mapper, IUnitOfWork uow) : base(mapper, uow) { }

        public List<UserDTO> GetAll()
        {
            return _mapper.Map<List<UserDTO>>(data.Users.GetAll());
        }

        public UserDTO Get(int id)
        {
            var user = data.Users.Get(id);
            return _mapper.Map<UserDTO>(user);
        }

        public UserDTO Add(UserDTO user)
        {
            var newUser = _mapper.Map<UserModel>(user);
            var addedUser = data.Users.Create(newUser);
            return _mapper.Map<UserDTO>(addedUser);
        }

        public UserDTO Update(UserDTO user)
        {
            var newUser = _mapper.Map<UserModel>(user);
            var updatedUser = data.Users.Update(newUser);
            return _mapper.Map<UserDTO>(updatedUser);
        }

        public void Delete(int id)
        {
            data.Users.Delete(id);
        }
    }
}
