﻿using AutoMapper;
using Common.DTO;
using HW2.BLL.Interfaces;
using HW2.BLL.Services.Abstract;
using HW2.DAL.Models;
using HW2.DAL.UOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace HW2.BLL.Services
{
    public class ProjectService : BaseService, IService<ProjectDTO>
    {
        public ProjectService(IMapper mapper, IUnitOfWork uow) : base(mapper, uow) { }

        public List<ProjectDTO> GetAll()
        {
            return _mapper.Map<List<ProjectDTO>>(data.Projects.GetAll());
        }

        public ProjectDTO Get(int id)
        {
            var project = data.Projects.Get(id);
            return _mapper.Map<ProjectDTO>(project);
        }

        public ProjectDTO Add(ProjectDTO project)
        {
            var newProject = _mapper.Map<ProjectModel>(project);
            var addedProject = data.Projects.Create(newProject);
            return _mapper.Map<ProjectDTO>(addedProject);
        }

        public ProjectDTO Update(ProjectDTO project)
        {
            var newProject = _mapper.Map<ProjectModel>(project);
            var updatedProject = data.Projects.Update(newProject);
            return _mapper.Map<ProjectDTO>(updatedProject);
        }

        public void Delete(int id)
        {
            data.Projects.Delete(id);
        }
    }
}
