﻿using AutoMapper;
using Common.DTO;
using HW2.BLL.Interfaces;
using HW2.BLL.Services.Abstract;
using HW2.DAL.Models;
using HW2.DAL.UOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2.BLL.Services
{
    public class TeamService : BaseService, IService<TeamDTO>
    {
        public TeamService(IMapper mapper, IUnitOfWork uow) : base(mapper, uow) { }

        public List<TeamDTO> GetAll()
        {
            return _mapper.Map<List<TeamDTO>>(data.Teams.GetAll());
        }

        public TeamDTO Get(int id)
        {
            var team = data.Teams.Get(id);
            return _mapper.Map<TeamDTO>(team);
        }

        public TeamDTO Add(TeamDTO team)
        {
            var newTeam = _mapper.Map<TeamModel>(team);
            var addedTeam = data.Teams.Create(newTeam);
            return _mapper.Map<TeamDTO>(addedTeam);
        }

        public TeamDTO Update(TeamDTO team)
        {
            var newTeam = _mapper.Map<TeamModel>(team);
            var updatedTeam = data.Teams.Update(newTeam);
            return _mapper.Map<TeamDTO>(updatedTeam);
        }

        public void Delete(int id)
        {
            data.Teams.Delete(id);
        }
    }
}
