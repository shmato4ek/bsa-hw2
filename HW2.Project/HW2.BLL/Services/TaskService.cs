﻿using AutoMapper;
using Common.DTO;
using HW2.BLL.Interfaces;
using HW2.BLL.Services.Abstract;
using HW2.DAL.Models;
using HW2.DAL.UOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2.BLL.Services
{
    public class TaskService : BaseService, IService<TaskDTO>
    {
        public TaskService(IMapper mapper, IUnitOfWork uow) : base(mapper, uow) { }

        public List<TaskDTO> GetAll()
        {
            return _mapper.Map<List<TaskDTO>>(data.Tasks.GetAll());
        }

        public TaskDTO Get(int id)
        {
            var task = data.Tasks.Get(id);
            return _mapper.Map<TaskDTO>(task);
        }

        public TaskDTO Add(TaskDTO task)
        {
            var newTask = _mapper.Map<TaskModel>(task);
            var addedTask = data.Tasks.Create(newTask);
            return _mapper.Map<TaskDTO>(addedTask);
        }

        public TaskDTO Update(TaskDTO task)
        {
            var newTask = _mapper.Map<TaskModel>(task);
            var updatedTask = data.Tasks.Update(newTask);
            return _mapper.Map<TaskDTO>(updatedTask);
        }

        public void Delete(int id)
        {
            data.Tasks.Delete(id);
        }
    }
}
