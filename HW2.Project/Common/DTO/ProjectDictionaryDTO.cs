﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO
{
    public class ProjectDictionaryDTO
    {
        public ProjectDTO Key { get; set; }
        public int Value { get; set; }
    }
}
