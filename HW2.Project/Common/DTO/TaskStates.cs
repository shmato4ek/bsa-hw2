﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO
{
    public enum TaskStates
    {
        Canceled = 0,
        Started,
        Finished,
        Stopped
    }
}
