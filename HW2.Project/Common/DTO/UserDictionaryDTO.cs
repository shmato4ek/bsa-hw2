﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTO
{
    public class UserDictionaryDTO
    {
        public UserDTO Key { get; set; }
        public List<TaskDTO> Value{ get; set; }
    }
}
