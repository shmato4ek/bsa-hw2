﻿using Common.DTO;
using HW2.BLL.Interfaces;
using HW2.BLL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HW2.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public ActionResult<List<UserDTO>> Get()
        {
            return Ok(_userService.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<UserDTO> GetById(int id)
        {
            try
            {
                var result = _userService.Get(id);
                return Ok(result);
            }
            catch(Exception)
            {
                throw new Exception($"User with id {id} was not found");
            }
        }

        [HttpPost]
        public ActionResult<UserDTO> CreateUser([FromBody] UserDTO user)
        {
            try
            {
                var newUser = _userService.Add(user);
                return Created($"~api/user/{newUser.Id}", newUser);
            }
            catch(Exception)
            {
                throw new Exception("This structure of user is uncorrect");
            }
        }

        [HttpPut]
        public ActionResult<UserDTO> UpdateUser([FromBody] UserDTO user)
        {
            try
            {
                var updatedUser = _userService.Update(user);
                return Created($"~api/team/{updatedUser.Id}", updatedUser);
            }
            catch(Exception)
            {
                throw new Exception("This structure of user is uncorrect");
            }
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteUser(int id)
        {
            try
            {
                _userService.Delete(id);
                return NoContent();
            }
            catch(Exception)
            {
                throw new Exception($"User with id {id} was not found");
            }
        }
    }
}
