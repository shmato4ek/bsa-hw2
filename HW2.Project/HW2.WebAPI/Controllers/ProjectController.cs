﻿using Common.DTO;
using HW2.BLL.Interfaces;
using HW2.BLL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HW2.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly ProjectService _projectService;

        public ProjectController(ProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public ActionResult<List<ProjectDTO>> Get()
        {
            return Ok(_projectService.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> GetById(int id)
        {
            try
            {
                var result = _projectService.Get(id);
                return Ok(result);
            }
            catch(Exception)
            {
                throw new Exception($"Project with id {id} was not found");
            }
        }

        [HttpPost]
        public ActionResult<ProjectDTO> CreateProject([FromBody] ProjectDTO project)
        {
            try
            {
                var newProject = _projectService.Add(project);
                return Created($"~api/project/{newProject.Id}", newProject);
            }
            catch(Exception)
            {
                throw new Exception("This structure of project is uncorrect");
            }
        }

        [HttpPut]
        public ActionResult<ProjectDTO> UpdateProject([FromBody] ProjectDTO project)
        {
            try
            {
                var updatedProject = _projectService.Update(project);
                return Created($"~api/project/{updatedProject.Id}", updatedProject);
            }
            catch(Exception)
            {
                throw new Exception("This structure of project is uncorrect");
            }
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteProject(int id)
        {
            try
            {
                _projectService.Delete(id);
                return NoContent();
            }
            catch(Exception)
            {
                throw new Exception($"Project with id {id} was not found");
            }
        }
    }
}
