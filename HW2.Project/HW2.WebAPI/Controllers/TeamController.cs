﻿using Common.DTO;
using HW2.BLL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HW2.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly TeamService _teamService;

        public TeamController(TeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public ActionResult<List<TeamDTO>> Get()
        {
            return _teamService.GetAll();
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDTO> GetById(int id)
        {
            try
            {
                var result = _teamService.Get(id);
                return Ok(result);
            }
            catch(Exception)
            {
                throw new Exception($"Team with id {id} was not found");
            }
        }

        [HttpPost]
        public ActionResult<TeamDTO> CreateTeam([FromBody] TeamDTO team)
        {
            try
            {
                var newTeam = _teamService.Add(team);
                return Created($"~api/team/{newTeam.Id}", newTeam);
            }
            catch(Exception)
            {
                throw new Exception("This structure of team is uncorrect");
            }
        }

        [HttpPut]
        public ActionResult<TeamDTO> UpdateTeam([FromBody] TeamDTO team)
        {
            try
            {
                var updatedTeam = _teamService.Update(team);
                return Created($"~api/team/{updatedTeam.Id}", updatedTeam);
            }
            catch(Exception)
            {
                throw new Exception("This structure of team is uncorrect");
            }
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteTeam(int id)
        {
            try
            {
                _teamService.Delete(id);
                return NoContent();
            }
            catch(Exception)
            {
                throw new Exception($"Task with id {id} was not found");
            }
        }
    }
}
