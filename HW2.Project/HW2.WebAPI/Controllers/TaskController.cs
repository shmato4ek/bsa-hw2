﻿using Common.DTO;
using HW2.BLL.Interfaces;
using HW2.BLL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HW2.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly TaskService _taskService;

        public TaskController(TaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public ActionResult<List<TaskDTO>> Get()
        {
            return Ok(_taskService.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<TaskDTO> GetById(int id)
        {
            try
            {
                var result = _taskService.Get(id);
                if(result is null)
                {
                    throw new Exception();
                }
                return Ok(result);
            }
            catch(Exception)
            {
                throw new Exception($"Task with id {id} was not found");
            }
        }

        [HttpPost]
        public ActionResult<TaskDTO> CreateTask([FromBody] TaskDTO task)
        {
            try
            {
                var newTask = _taskService.Add(task);
                return Created($"~api/task/{newTask.Id}", newTask);
            }
            catch(Exception)
            {
                throw new Exception("This structure of task is uncorrect");
            }
        }

        [HttpPut]
        public ActionResult<TaskDTO> UpdateTask([FromBody] TaskDTO task)
        {
            try
            {
                var updatedTask = _taskService.Update(task);
                return Created($"~api/task/{updatedTask.Id}", updatedTask);
            }
            catch(Exception)
            {
                throw new Exception("This structure of task is uncorrect");
            }
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteTask(int id)
        {
            try
            {
                _taskService.Delete(id);
                return NoContent();
            }
            catch
            {
                throw new Exception($"Task with id {id} was not found");
            }
        }
    }
}
