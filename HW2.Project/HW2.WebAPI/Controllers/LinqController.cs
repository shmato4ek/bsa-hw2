﻿using AutoMapper;
using Common.DTO;
using HW1.BLL.ModelsForLINQ;
using HW2.BLL.Services;
using HW2.BLL.Structs;
using HW2.WebAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HW2.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly LinqService _linqService;
        public LinqController(LinqService linqService)
        {
            _linqService = linqService;
        }

        [HttpGet("user/{id}/tasks/amount")]
        public ActionResult<List<ProjectDictionaryDTO>> GetAmmountOfTasksOfConcreteUser(int id)
        {
            try
            {
                var result = _linqService.GetAmmountOfTasksOfConcreteUser(id);
                return Ok(result);
            }
            catch (Exception)
            {
                throw new Exception($"User with id {id} was not found");
            }
        }

        [HttpGet("user/{id}/tasks")]
        public ActionResult<List<TaskDTO>> GetAllTasksOfConcreteUser(int id)
        {
            try
            {
                var result = _linqService.GetAllTasksOfConcreteUser(id);
                return Ok(result);
            }
            catch(Exception)
            {
                throw new Exception($"User with id {id} was not found");
            }
        }

        [HttpGet("user/{id}/tasks/finished")]
        public ActionResult<List<TaskDictionaryDTO>> GetFinishedTasksOfConcreteUser(int id)
        {
            try
            {
                var result = _linqService.GetFinishedTasksOfConcreteUser(id);
                return Ok(result);
            }
            catch(Exception)
            {
                throw new Exception($"User with id {id} was not found");
            }
        }

        [HttpGet("teams/older-then10")]
        public ActionResult<List<TeamWithUsersStruct>> GetTeamsWithUsersOlderThan10()
        {
            var result = _linqService.GetTeamsWithUsersOlderThan10();
            return Ok(result);
        }

        [HttpGet("users/sorted")]
        public ActionResult<List<UserDictionaryDTO>> GetSortedListOfUsers()
        {
            var result = _linqService.GetSortedListOfUsers();
            return Ok(result);
        }

        [HttpGet("user/{id}/info")]
        public ActionResult<UserStructDTO> GetUserStruct(int id)
        {
            try
            {
                var result = _linqService.GetUserStruct(id);
                return Ok(result);
            }
            catch(Exception)
            {
                throw new Exception($"User with id {id} was not found");
            }
        }

        [HttpGet("projects/info")]
        public ActionResult<List<ProjectStruct>> GetProjectStruct()
        {
            var result = _linqService.GetProjectStruct();
            return Ok(result);
        }
    }
}
