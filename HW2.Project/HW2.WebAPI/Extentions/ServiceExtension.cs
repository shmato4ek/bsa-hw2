﻿using HW2.BLL.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using HW2.BLL.MappingProfiles;
using System.Reflection;
using HW2.DAL.UOW;

namespace HW2.WebAPI.Extentions
{
    public static class ServiceExtension
    {
        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddScoped<DataService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ProjectService>();
            services.AddScoped<LinqService>();
            services.AddScoped<TaskService>();
            services.AddScoped<TeamService>();
            services.AddScoped<UserService>();
        }

        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TaskForLinqProfile>();
                cfg.AddProfile<ProjectLinqProfile>();
                cfg.AddProfile<UserStructProfile>();
                cfg.AddProfile<UserForLinqProfile>();
            },
            Assembly.GetExecutingAssembly());
        }
    }
}
